ko.bindingHandlers.modal = {
  init: function(element, valueAccessor) {
    var value = valueAccessor();
    $(element).on('hidden.bs.modal', function() {
      if (ko.isObservable(value)) {
        value(false);
      }
    });
    $(element).modal({backdrop: 'static', keyboard: true, show: false});
  },
  update: function(element, valueAccessor) {
    var value = valueAccessor();
    if (ko.utils.unwrapObservable(value)) {
      $(element).modal('show');
      $('input', element).focus();
    }
    else {
      $(element).modal('hide');
    }
  }
};