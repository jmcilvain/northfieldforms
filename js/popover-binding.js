$('body').on('click', 'button[data-dismiss="popover"]', function(event) {
  var target = event.target; // button that was clicked

  // find parent popover
  var popover = $(target).parents('.popover');
  $.each(popover, function(i, element) {
    var context = ko.contextFor(element);
    context.$rawData(false);
  });
});

ko.bindingHandlers.popover = {
  init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var options = ko.unwrap(valueAccessor());

    var trigger = options.with;
    if (!ko.isObservable(trigger)) {
      throw new Error('trigger must be observable');
    }

    var title = ko.unwrap(options.title);
    var content = ko.unwrap(options.content);
    var titleSelector = ko.unwrap(options.titleSelector);
    var contentSelector = ko.unwrap(options.contentSelector);
    var titleHtml = $(titleSelector).html() || title;
    var contentHtml = $(contentSelector).html() || content;
    var target = $(element).popover({trigger: 'manual', html: true, placement: ko.unwrap(options.placement), title: titleHtml, content: contentHtml});

    target.on('shown.bs.popover', function() {
      var popover = target.next('.popover');
      if (!popover.length) return;

      // bind content
      ko.cleanNode(popover[0]);
      var context = bindingContext.createChildContext(trigger);
      ko.applyBindings(context, popover[0]);

      // set focus to first input
      $(':input:visible:enabled:first', popover).focus();
      $(':input:visible:enabled:first', popover).select();
    });

    target.on('hide.bs.popover', function() {
      var popover = target.next('.popover');
      if (popover.length) {
        ko.cleanNode(popover[0]);
      }
    });

    target.on('hidden.bs.popover', function() {
      trigger(false);
    });

    trigger.subscribe(function(value) {
      if (value) {
        target.popover('show');
      } else {
        target.popover('hide');
      }
    });

    //handle disposal (if KO removes by the template binding)
    ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
      $(element).popover('destroy');
    });
  }
};